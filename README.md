# GRC - Git Remote Creation (JS)
A simple tool to create your bitbucket and github remotes.

## Install
``` bash
npm install grc

```


## Getting Started

Here's what you need to get started:

- A github and/or bitbucket account
- personal access token (for github auth)
- git

### Instructions

#### Github

1. Go to [github](https://github.com) and login/create account
2. Create a [token](https://github.com/settings/tokens)
    * *Give scope **repo** for public and private repo access*
    * *Give scope **pubilc_repo** for only public repo access*
3. Use flags -u and -t to specify Username and Token (-t shows your token)
    * *without the -t flag you will be prompted for your password*
    * *without the -u flag, the value from 'git config --get user.username' is used*
4. Use the --g flag to use github

#### Bitbucket

1. Go to [bitbucket](https://bitbucket.org) and login/create account.
2. Use flags -u and -t to specify Username and Password (-t shows your token)
    * *without the the -t flag you will be prompted for your password*
    * *without the -u flag, the value from 'git config --get user.username' is used*

###Flags
* --g - Set host to github
* -n - Name of repo, defaults to name of current directory
* --p - Set repo to private
* -t - Token for respective host
* -u - Username for respective host, defaults to git username
* -s - Team name if creating a team repository (bitbucket only)
